# LEAP-climsim-infra

This repository contains the infrastructure for the LEAP climsim project. It is a collection of Terraform modules that can be used to deploy the necessary infrastructure for the project.